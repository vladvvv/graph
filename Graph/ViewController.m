//
//  ViewController.m
//  Graph
//
//  Created by Владислав Волобуев on 8/1/16.
//  Copyright © 2016 Владислав Волобуев. All rights reserved.
//

#import "ViewController.h"
#import "GraphView.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet GraphView *graph;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *data = @[@1.0f, @1.0f, @1.0f, @1.0f, @1.0f, @1.0f, @0.25f, @0.25f, @0.75f, @0.75f, @0.75f, @0.75f, @0.75f, @1.0f, @1.0f, @1.0f, @0.75f, @0.75f, @0.75f, @0.75f, @0.75f, @0.5f, @0.5f, @0.5f, @0.5f,@0.5f, @0.5f, @0.5f, @0.5f, @0.5f];
    self.graph.data = data;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
