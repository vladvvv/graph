//
//  main.m
//  Graph
//
//  Created by Владислав Волобуев on 8/1/16.
//  Copyright © 2016 Владислав Волобуев. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
