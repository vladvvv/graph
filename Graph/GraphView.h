//
//  GraphView.h
//  Graph
//
//  Created by Владислав Волобуев on 8/1/16.
//  Copyright © 2016 Владислав Волобуев. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kGraphHeight 100
#define kDefaultGraphWidth 400
#define kGraphBottom 100
#define kGraphTop 0
#define kOffsetX 20
#define kOffsetY 0
#define kStepX 10
#define kStepY 25

@interface GraphView : UIView

@property (strong, nonatomic) NSArray *data;

@end
