//
//  GraphView.m
//  Graph
//
//  Created by Владислав Волобуев on 8/1/16.
//  Copyright © 2016 Владислав Волобуев. All rights reserved.
//

#import "GraphView.h"

@implementation GraphView

- (void)drawLineGraphWithContext:(CGContextRef)context
{
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);
    
    int maxGraphHeight = kGraphHeight - kOffsetY;
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, kOffsetX, kGraphHeight - maxGraphHeight * [self.data[0] floatValue]+kStepY/2);
    
    float t = [self.data[0] floatValue];
    int k = 1;
    for (int i = 1; i < self.data.count; i++)
    {
        k++;
        if ([self.data[i] floatValue] != t) {
            t = [self.data[i] floatValue];
            k--;
        }
        CGContextAddLineToPoint(context, kOffsetX + k * kStepX, kGraphHeight - maxGraphHeight * [self.data[i] floatValue] + kStepY/2);
    }
    CGContextDrawPath(context, kCGPathStroke);
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 0.6);
    CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
    
    int verticalLinesCount = (kDefaultGraphWidth - kOffsetX) / kStepX;

    for (int i = 0; i < verticalLinesCount; i++)
    {
        CGContextMoveToPoint(context, kOffsetX + i * kStepX, kGraphTop);
        CGContextAddLineToPoint(context, kOffsetX + i * kStepX, kGraphBottom);
    }
    
    int horizontalLinesCount = (kGraphBottom - kGraphTop - kOffsetY) / kStepY;
    for (int i = 0; i <= horizontalLinesCount; i++)
    {
        CGContextMoveToPoint(context, kOffsetX, kGraphBottom - kOffsetY - i * kStepY);
        CGContextAddLineToPoint(context, kDefaultGraphWidth, kGraphBottom - kOffsetY - i * kStepY);
    }
    
    CGContextStrokePath(context);
    [self drawLineGraphWithContext:context];
    
    CGContextSetTextDrawingMode(context, kCGTextFill);
    [[UIColor blackColor] setFill];
    
    for (int i = 1; i < self.data.count; i++)
    {
        NSString *text = [NSString stringWithFormat:@"%d", i];
        [text drawAtPoint:CGPointMake( kOffsetX + i * kStepX-2, kGraphBottom+2) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:7]}];
    }
    
    
    [@"OFF" drawAtPoint:CGPointMake( 0, kStepY - kStepY/2)
          withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica"
                                                               size:7]
                           }];
    [@"SB" drawAtPoint:CGPointMake( 0, 2*kStepY - kStepY/2)
         withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica"
                                                              size:7]
                          }];
    [@"DR" drawAtPoint:CGPointMake( 0, 3*kStepY - kStepY/2)
         withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica"
                                                              size:7]
                          }];
    [@"ON" drawAtPoint:CGPointMake( 0, 4*kStepY - kStepY/2)
         withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica"
                                                              size:7]
                          }];
}


@end
