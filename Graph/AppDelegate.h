//
//  AppDelegate.h
//  Graph
//
//  Created by Владислав Волобуев on 8/1/16.
//  Copyright © 2016 Владислав Волобуев. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

